/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import org.aspectj.lang.ProceedingJoinPoint;

import java.util.Deque;
import java.util.LinkedList;

public abstract class AbstractCallRecorder {

    private static ThreadLocal<Deque<Call>> callStackHolder = new ThreadLocal<>();
    private boolean enabled = true;

    @SuppressWarnings("unused")
    public boolean isEnabled() {
        return enabled;
    }

    @SuppressWarnings("unused")
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    protected void push(Call call) {
        Deque<Call> callStack = callStackHolder.get();
        if (callStack == null) {
            callStack = new LinkedList<>();
            callStackHolder.set(callStack);
        }
        callStack.push(call);
    }

    protected Call pop() {
        return callStackHolder.get().pop();
    }

    protected Call peek() {
        Deque<Call> deque = callStackHolder.get();
        if (deque == null) {
            return null;
        }
        return deque.peek();
    }


    protected Call recordAndPerformCall(ProceedingJoinPoint jp) throws Throwable {
        Call call = new Call();

        push(call);

        call.arguments = jp.getArgs();
        call.methodName = jp.getSignature().getName();
        call.className = jp.getTarget().getClass().getCanonicalName();
        try {
            call.returnedValue = jp.proceed();
        } catch (Throwable throwable) {
            call.returnedThrowable = throwable;
        }
        call.completionTimestamp = System.currentTimeMillis();

        pop();

        return call;
    }

    protected static Object returnCall(Call call) throws Throwable {
        if (call.returnedThrowable != null) {
            throw call.returnedThrowable;
        }
        return call.returnedValue;
    }
}

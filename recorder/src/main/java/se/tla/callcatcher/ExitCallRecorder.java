/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import org.aspectj.lang.ProceedingJoinPoint;

public class ExitCallRecorder extends AbstractCallRecorder {

    private boolean childCallsAreOptional = false;
    /**
     * Method called during AOP processing to record the a call to an exit point.
     * @param pjp As per the AOP API.
     * @return The result of the call as per the AOP API.
     * @throws Throwable As per the AOP API.
     */
    public Object handleExitpoint(ProceedingJoinPoint pjp) throws Throwable {
        if (!isEnabled()) {
            return pjp.proceed();
        }

        if (peek() == null) {
            return pjp.proceed();
        }

        Call call = recordAndPerformCall(pjp);

        call.childCallsAreOptional = childCallsAreOptional;
        // Add it as a performed child call to parent.
        peek().childCalls.add(call);

        return returnCall(call);
    }

    public void setChildCallsAreOptional(boolean childCallsAreOptional) {
        this.childCallsAreOptional = childCallsAreOptional;
    }
}

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class FileRecordingSaver implements RecordingSaver {
    private String saveDir;

    @Override
    public void save(byte[] buffer) {
        try {
            FileUtils.writeByteArrayToFile(new File(generateFQFilename()), buffer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws IOException {
        // Nothing to do.
    }

    protected String generateFQFilename() {
        return saveDir + generateLocalFilename();
    }

    protected static String generateLocalFilename() {
        return System.currentTimeMillis() + "-" + UUID.randomUUID().toString();
    }

    @SuppressWarnings("unused")
    public String getSaveDir() {
        return saveDir;
    }

    @SuppressWarnings("unused")
    public void setSaveDir(String saveDir) {
        if (! new File(saveDir).isDirectory()) {
            throw new IllegalArgumentException("No such directory" + saveDir);
        }

        String slash = "";
        if (! saveDir.endsWith("/")) {
            slash = "/";
        }

        this.saveDir = saveDir + slash;
    }
}

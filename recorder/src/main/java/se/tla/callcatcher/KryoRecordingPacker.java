/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;

import java.io.IOException;
import java.util.Arrays;

public class KryoRecordingPacker implements RecordingPacker {
    private RecordingSaver recordingSaver;
    private Kryo kryo = new Kryo();

    @Override
    public void pack(Call call) {
        Output output = new Output(1024, 1024 * 64);
        kryo.writeObject(output, call);
        recordingSaver.save(Arrays.copyOf(output.getBuffer(), (int) output.total()));
    }

    @Override
    public void close() throws IOException {
        recordingSaver.close();
    }

    @SuppressWarnings("unused")
    public RecordingSaver getRecordingSaver() {
        return recordingSaver;
    }

    @SuppressWarnings("unused")
    public void setRecordingSaver(RecordingSaver recordingSaver) {
        this.recordingSaver = recordingSaver;
    }
}

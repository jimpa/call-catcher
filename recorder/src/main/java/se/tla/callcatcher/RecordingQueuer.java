/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class RecordingQueuer {
    private ThreadPoolExecutor executor;
    private RecordingPacker recordingPacker;
    private int queueLength;

    @PostConstruct
    public void init() {
        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>(queueLength);
        executor = new ThreadPoolExecutor(1, 1, 1, TimeUnit.MINUTES, queue);
        // If queue is full, always reject the next CallGraph and communicate this with an exception.
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
    }

    @PreDestroy
    public void destroy() throws InterruptedException, IOException {
        executor.shutdown();
        executor.awaitTermination(30, TimeUnit.SECONDS);
        recordingPacker.close();
    }

    public boolean queueCallChain(Call call) {
        try {
            executor.execute(new CallChainSaver(call));
            return true;
        } catch (RejectedExecutionException e) {
            return false;
        }
    }

    class CallChainSaver implements Runnable {

        private final Call call;

        public CallChainSaver(Call call) {
            this.call = call;
        }

        @Override
        public void run() {
            recordingPacker.pack(call);
        }
    }

    @SuppressWarnings("unused")
    public RecordingPacker getRecordingPacker() {
        return recordingPacker;
    }

    public void setRecordingPacker(RecordingPacker recordingPacker) {
        this.recordingPacker = recordingPacker;
    }

    @SuppressWarnings("unused")
    public int getQueueLength() {
        return queueLength;
    }

    public void setQueueLength(int queueLength) {
        this.queueLength = queueLength;
    }
}

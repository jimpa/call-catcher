/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarHeader;
import org.kamranzafar.jtar.TarOutputStream;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class TarFileRecordingSaver extends FileRecordingSaver {

    private ExtendedTarOutputStream tarOutputStream;

    @Override
    public void save(byte[] buffer) {
        try {
            ExtendedTarOutputStream tos = getTarOutputStream();
            tos.putNextEntry(new TarEntry(TarHeader.createHeader(generateLocalFilename(), buffer.length, System.currentTimeMillis() / 1000, false)));
            tos.write(buffer);
            tos.finishEntry();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public ExtendedTarOutputStream getTarOutputStream() throws FileNotFoundException {
        synchronized (this) {
            if (tarOutputStream == null) {
                tarOutputStream = new ExtendedTarOutputStream(new BufferedOutputStream(new FileOutputStream(generateFQFilename())));
            }
        }
        return tarOutputStream;
    }

    @Override
    public void close() throws IOException {
        if (tarOutputStream != null) {
            tarOutputStream.close();
        }
    }
}

class ExtendedTarOutputStream extends TarOutputStream {

    public ExtendedTarOutputStream(OutputStream out) {
        super(out);
    }

    public void finishEntry() {
        try {
            closeCurrentEntry();
            flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

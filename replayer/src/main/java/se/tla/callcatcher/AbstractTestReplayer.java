/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import java.util.Iterator;

import static org.junit.Assert.fail;

public abstract class AbstractTestReplayer extends AbstractJUnit4SpringContextTests {

    @Autowired
    private RecordingUnpacker recordingUnpacker;
    @Autowired
    private CallReplayer replayer;

    @Test
    public void replay() {
        replayer.setReplayFailureHandler(new JUnitReplayFailureHandler());
        for (Iterator<Recording> it = recordingUnpacker.recordingsIterator() ; it.hasNext(); ) {
            replayer.replay(it.next());
        }
    }
}

class JUnitReplayFailureHandler implements ReplayFailureHandler {

    @Override
    public void entryPointUnavailable(Recording recording, String className, Exception e) {
        fail(ctxMsg(recording) + "Entrypoint can't be found: " + className + ". Caused by: " + e.getMessage());
    }

    @Override
    public void entryPointUnavailable(Recording recording, String className) {
        fail(ctxMsg(recording) + "Entrypoint can't be found: " + className);
    }

    @Override
    public void entrypointReturnedWithUnexpectedException(Recording recording, Throwable t) {
        fail(ctxMsg(recording) + "Entrypoint returned with unexpected Exception" + t.getMessage());
    }

    @Override
    public void entrypointReturnedWrongResult(Recording recording, String message) {
        fail(ctxMsg(recording) + "Entry point returned with wrong result: " + message);
    }

    @Override
    public void unexpectedExitpointCalled(Recording recording,
                                          String expectedClassname, String actualClassname,
                                          String expectedMethodname, String actualMethodname) {
        fail(ctxMsg(recording) + "Exit call doesn't match next recorded call. " +
                "Expected class: " + expectedClassname + ", got " + actualClassname +
                "Expected method: " + expectedMethodname + ", got " + actualMethodname);
    }

    @Override
    public void unexpectedExitpointCalled(Recording recording) {
        fail(ctxMsg(recording) + "Couldn't find the matching call");
    }

    @Override
    public void exitpointCalledWithWrongArguments(Recording recording, String message) {
        fail(ctxMsg(recording) + message);
    }

    @Override
    public void exitpointWasNotCalled(Recording recording) {
        fail(ctxMsg(recording) + "All exitponts was not called.");
    }

    @Override
    public void entrypointReturnedWithoutExpectedException(Recording recording) {
        fail(ctxMsg(recording) + "Entrypoint returned without its expected Exception.");
    }

    @Override
    public void internalComparisonFailure(Recording recording, Exception e) {
        fail(ctxMsg(recording) + "Internal failure comparing results" + e.getMessage());
    }

    private String ctxMsg(Recording recording) {
        return "(" + recording.getName() + ") ";
    }
}

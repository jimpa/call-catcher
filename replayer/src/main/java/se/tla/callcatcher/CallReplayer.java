/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import org.aspectj.lang.ProceedingJoinPoint;
import org.custommonkey.xmlunit.Diff;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class CallReplayer {

    private RecordingKeeper recordingKeeper;
    private static final Logger logger = LoggerFactory.getLogger(CallReplayer.class);

    private XStream xstream = new XStream(new StaxDriver());

    @Autowired
    private ApplicationContext applicationContext;

    private ReplayFailureHandler replayFailureHandler = new LoggingOnlyFailureHandler();

    public void replay(Recording recording) {
        recordingKeeper = new RecordingKeeper(recording);
        Call entryCall = recording.getEntryCall();
        Object entryPointObject;
        try {
            entryPointObject = applicationContext.getBean(Class.forName(entryCall.className));
        } catch (ClassNotFoundException e) {
            replayFailureHandler.entryPointUnavailable(recording, entryCall.className, e);
            return;
        }
        if (entryPointObject == null) {
            replayFailureHandler.entryPointUnavailable(recording, entryCall.className);
            return;
        }

        recordingKeeper.callStack.push(new StackItem(entryCall));

        List<Class> argumentClasses = new ArrayList<>();
        for (Object o : entryCall.arguments) {
            argumentClasses.add(o.getClass());
        }
        try {
            Method method = entryPointObject.getClass().getDeclaredMethod(entryCall.methodName,
                    argumentClasses.toArray(new Class[argumentClasses.size()]));
            if (entryCall.returnedThrowable == null) {
                // Expects a normal return
                logger.info("CallReplayer.replay entry call: {}.{}", entryCall.className, entryCall.methodName);
                Object result;
                try {
                    result = method.invoke(entryPointObject, entryCall.arguments);
                } catch (Throwable t) {
                    replayFailureHandler.entrypointReturnedWithUnexpectedException(recording, t);
                    return;
                }
                ensureReturnValueEquality(entryCall.returnedValue, result);
            } else {
                // Expects an Exception
                logger.info("CallReplayer.replay entry call: {}.{}", entryCall.className, entryCall.methodName);
                try {
                    method.invoke(entryPointObject, entryCall.arguments);
                    replayFailureHandler.entrypointReturnedWithoutExpectedException(recording);
                } catch (Throwable t) {
                    if (!equalThrowables(t.getCause(), entryCall.returnedThrowable)) {
                        replayFailureHandler.entrypointReturnedWithUnexpectedException(recording, t);
                    }
                }
            }
        } catch (NoSuchMethodException e) {
            replayFailureHandler.entryPointUnavailable(recording, entryCall.className, e);
        }
    }

    /**
     * This is where all the calls to the exit points defined in the AOP pointcuts gets intercepted.
     */
    public Object handleExitpoint(ProceedingJoinPoint pjp) throws Throwable {
        Call call = recordingKeeper.getNextExitCall();
        if (!matches(call, pjp)) {
            replayFailureHandler.unexpectedExitpointCalled(recordingKeeper.getRecording(),
                    call.className, pjp.getTarget().getClass().getCanonicalName(),
                    call.methodName, pjp.getSignature().getName());
        }
        ensureArgumentsEquality(call.arguments, pjp.getArgs());

        // If the call don't have any child calls, return directly according to recording.
        if (call.childCalls.isEmpty()) {
            if (call.returnedThrowable != null) {
                throw call.returnedThrowable;
            }
            return call.returnedValue;
        }

        recordingKeeper.callStack.push(new StackItem(call));
        // Wrap and catch possible exceptions.
        // Catch return value
        Throwable thrown = null;
        Object result = null;
        try {
            result = pjp.proceed();
        } catch (Throwable t) {
            thrown = t;
        }
        StackItem stackItem = recordingKeeper.callStack.pop();

        // If the calls to the children are optional, don't check if they were performed.
        if (! call.childCallsAreOptional) {
            if (stackItem.calls.hasNext()) {
                replayFailureHandler.exitpointWasNotCalled(recordingKeeper.recording);
            }
        }

        logger.info("Faking call to: {}.{}", call.className, call.methodName);
        if (thrown != null) {
            throw thrown;
        }
        return result;
    }

    private boolean matches(Call call, ProceedingJoinPoint pjp) {
        return
                call.className.equals(pjp.getTarget().getClass().getCanonicalName()) &&
                call.methodName.equals(pjp.getSignature().getName());
    }

    private void ensureReturnValueEquality(Object expected, Object actual) {
        Diff diff = determineObjectEquality(expected, actual);
        if (diff != null && !diff.identical()) {
            replayFailureHandler.entrypointReturnedWrongResult(recordingKeeper.getRecording(),
                    diff.appendMessage(new StringBuffer()).toString());
        }
    }

    private void ensureArgumentsEquality(Object[] expected, Object[] actual) {
        if (expected.length != actual.length) {
            replayFailureHandler.exitpointCalledWithWrongArguments(recordingKeeper.getRecording(),
                    "Wrong number of arguments. Expected " +
                    expected.length + " got " + actual.length);
        }
        StringBuffer results = new StringBuffer();
        for (int i = 0; i < expected.length; i++) {
            Diff diff = determineObjectEquality(expected[i], actual[i]);
            if (diff != null && !diff.identical()) {
                diff.appendMessage(results);
            }
        }

        if (results.length() != 0) {
            replayFailureHandler.exitpointCalledWithWrongArguments(recordingKeeper.getRecording(),
                    "Argument comparison failed: " + results.toString());
        }
    }

    private Diff determineObjectEquality(Object expected, Object actual) {
        String expectedXml = xstream.toXML(expected);
        String actualXML = xstream.toXML(actual);

        try {
            return new Diff(expectedXml, actualXML);
        } catch (SAXException | IOException e) {
            replayFailureHandler.internalComparisonFailure(recordingKeeper.getRecording(), e);
            return null;
        }
    }

    private boolean equalThrowables(Throwable throwable1, Throwable throwable2) {
        return throwable1.getClass().equals(throwable2.getClass()) &&
               throwable1.getMessage().equals(throwable2.getMessage());
    }

    public void setReplayFailureHandler(ReplayFailureHandler replayFailureHandler) {
        this.replayFailureHandler = replayFailureHandler;
    }

    class RecordingKeeper {
        final private Recording recording;
        final Deque<StackItem> callStack = new LinkedList<>();

        public RecordingKeeper(Recording recording) {
            this.recording = recording;
        }

        public Call getNextExitCall() {
            Iterator<Call> callIterator = callStack.peek().calls;

            if (callIterator.hasNext()) {
                return callIterator.next();
            }

            replayFailureHandler.unexpectedExitpointCalled(recordingKeeper.getRecording());
            throw new IllegalStateException("Couldn't find the matching call");
        }

        public Recording getRecording() {
            return recording;
        }
    }

    static class StackItem {
        final Call call;
        final Iterator<Call> calls;

        public StackItem(Call call) {
            this.call = call;
            calls = call.childCalls.iterator();
        }
    }

    class LoggingOnlyFailureHandler implements ReplayFailureHandler {

        @Override
        public void entryPointUnavailable(Recording recording, String className, Exception e) {
            logger.error(ctxMsg(recording) + "Entrypoint can't be found: " + className, e);
        }

        @Override
        public void entryPointUnavailable(Recording recording, String className) {
            logger.error(ctxMsg(recording) + "Entrypoint can't be found: " + className);
        }

        @Override
        public void entrypointReturnedWithUnexpectedException(Recording recording, Throwable t) {
            logger.error(ctxMsg(recording) + "Entrypoint returned with unexpected Exception", t);
        }

        @Override
        public void entrypointReturnedWrongResult(Recording recording, String message) {
            logger.error(ctxMsg(recording) + "Entry point returned with wrong result: " + message);
        }

        @Override
        public void unexpectedExitpointCalled(Recording recording,
                                              String expectedClassname, String actualClassname,
                                              String expectedMethodname, String actualMethodname) {
            logger.error(ctxMsg(recording) +
                    "Exit call doesn't match next recorded call. " +
                    "Expected class: " + expectedClassname + ", got " + actualClassname +
                    "Expected method: " + expectedMethodname + ", got " + actualMethodname);
        }

        @Override
        public void unexpectedExitpointCalled(Recording recording) {
            logger.error(ctxMsg(recording) + "Couldn't find the matching call");
        }

        @Override
        public void exitpointCalledWithWrongArguments(Recording recording, String message) {
            logger.error(message);
        }

        @Override
        public void exitpointWasNotCalled(Recording recording) {
            logger.error(ctxMsg(recording) + "All exitponts was not called.");
        }

        @Override
        public void entrypointReturnedWithoutExpectedException(Recording recording) {
            logger.error(ctxMsg(recording) + "Entrypoint returned without its expected Exception.");
        }

        @Override
        public void internalComparisonFailure(Recording recording, Exception e) {
            logger.error(ctxMsg(recording) + "Internal failure comparing results", e);
        }

        private String ctxMsg(Recording recording) {
            return "(" + recording.getName() + ") ";
        }
    }
}

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

public interface ReplayFailureHandler {
    void entryPointUnavailable(Recording recording, String className, Exception e);

    void entryPointUnavailable(Recording recording, String className);

    void entrypointReturnedWithUnexpectedException(Recording recording, Throwable t);

    void entrypointReturnedWrongResult(Recording recording, String message);

    void unexpectedExitpointCalled(Recording recording,
                                   String expectedClassname, String actualClassname,
                                   String expectedMethodname, String actualMethodname);

    void unexpectedExitpointCalled(Recording recording);

    void exitpointCalledWithWrongArguments(Recording recording, String message);

    void exitpointWasNotCalled(Recording recording);

    void entrypointReturnedWithoutExpectedException(Recording recording);

    void internalComparisonFailure(Recording recording, Exception e);
}

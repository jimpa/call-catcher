/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarInputStream;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class TarFileRecordingLoader implements RecordingLoader {
    private final Iterator<File> tarFiles;
    private Iterator<Recording> recordings;

    public TarFileRecordingLoader(String base) {
        try {
            tarFiles = new FileFinder().getFiles(base).iterator();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Iterator<Recording> getContentIterator() {
        return new Iterator<Recording>() {
            @Override
            public boolean hasNext() {
                return combinedHasNext();
            }

            @Override
            public Recording next() {
                if (hasNext()) {
                    return recordings.next();
                }
                throw new NoSuchElementException();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    protected boolean combinedHasNext() {
        // Does current tar file have more files stored?
        if (recordings != null && recordings.hasNext()) {
            return true;
        }

        // Lets move on to other tar files.
        while(tarFiles.hasNext()) {
            File nextTarFile = tarFiles.next();
            recordings = findRecordings(nextTarFile);
            if (recordings.hasNext()) {
                return true;
            }
        }

        return false;
    }

    private Iterator<Recording> findRecordings(File nextTarFile) {
        try {
            final TarInputStream tis = new TarInputStream(new BufferedInputStream(new FileInputStream(nextTarFile)));
            return new Iterator<Recording>() {
                TarEntry tarEntry = null;
                @Override
                public boolean hasNext() {
                    if (tarEntry != null) {
                        return true;
                    }
                    try {
                        tarEntry = tis.getNextEntry();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    return tarEntry != null;
                }

                @Override
                public Recording next() {
                    if (hasNext()) {
                        int size = (int) tarEntry.getSize();
                        byte[] buffer = new byte[size];
                        try {
                            if (tis.read(buffer, 0, size) != size) {
                                throw new MalformedTarFileException();
                            }
                            return new Recording().setBuffer(buffer).setName(tarEntry.getName());
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        } finally {
                            tarEntry = null;
                        }
                    }
                    throw new NoSuchElementException();
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private class MalformedTarFileException extends RuntimeException {
    }
}

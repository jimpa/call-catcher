/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import se.tla.callcatcher.domain.HelloService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FullHappyFlowTests {

    private static final Logger logger = LoggerFactory.getLogger(FullHappyFlowTests.class);

    @Test
    public void happyFlowWithStorageMocked() {
        // Part 1: Make a call through a recorded service
        AbstractApplicationContext readingContext = new ClassPathXmlApplicationContext("spring-recorder-kryo-test.xml");
        readingContext.registerShutdownHook();

        final List<Call> savedCalls = new LinkedList<>();
        EntryCallRecorder recorder = (EntryCallRecorder) readingContext.getBean("entryRecorder");
        recorder.setRecordingQueuer(new RecordingQueuer() {
            @Override
            public boolean queueCallChain(Call call) {
                savedCalls.add(call);
                return true;
            }
        });
        HelloService helloService = readingContext.getBean(HelloService.class);

        logger.info(helloService.sayGoodbye("Billy"));
        logger.info("done in main");
        readingContext.close();

        // Assert that a file was created
        assertEquals(1, savedCalls.size());

        // Part 2: Read the recorded call from part 1 and replay it in a new context.
        AbstractApplicationContext replayingContext = new ClassPathXmlApplicationContext("spring-replayer-kryo-test.xml");
        assertTrue(replayingContext.isActive());

        CallReplayer callReplayer = (CallReplayer) replayingContext.getBean("replayer");
        for (Call call : savedCalls) {
            callReplayer.replay(new Recording().setCallGraph(call).setName("Testrecording"));
        }

        replayingContext.close();
    }

    @Test
    public void happyFlowWithKryoStorage() throws InterruptedException {
        // Part 1: Make a call through a recorded service
        AbstractApplicationContext readingContext = new ClassPathXmlApplicationContext("spring-recorder-kryo-test.xml");
        readingContext.registerShutdownHook();
        KryoRecordingPacker recordingSaver = (KryoRecordingPacker) readingContext.getBean("recordingPacker");

        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final CountDownLatch latch = new CountDownLatch(1);
        recordingSaver.setRecordingSaver(new RecordingSaver() {
            @Override
            public void save(byte[] buffer) {
                try {
                    bos.write(buffer);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                latch.countDown();
            }

            @Override
            public void close() throws IOException { }
        });

        HelloService helloService = readingContext.getBean(HelloService.class);

        logger.info(helloService.sayGoodbye("Billy"));
        logger.info("done in main");
        readingContext.close();
        latch.await();

        // Assert that a file was created
        assertEquals(427, bos.size());

        // Part 2: Read the recorded call from part 1 and replay it in a new context.
        AbstractApplicationContext replayingContext = new ClassPathXmlApplicationContext("spring-replayer-kryo-test.xml");

        KryoRecordingUnpacker unpacker = new KryoRecordingUnpacker(new TestRecordingLoader("Testrecording", bos.toByteArray()));
        CallReplayer callReplayer = replayingContext.getBean(CallReplayer.class);
        for (Iterator<Recording> it = unpacker.recordingsIterator(); it.hasNext(); ) {
            callReplayer.replay(it.next());
        }

        replayingContext.close();
    }

    @Test
    public void happyFlowWithXStreamStorage() throws InterruptedException {

        // Part 1: Make a call through a recorded service
        AbstractApplicationContext readingContext = new ClassPathXmlApplicationContext("spring-recorder-xstream-test.xml");
        readingContext.registerShutdownHook();
        XStreamRecordingPacker recordingSaver = (XStreamRecordingPacker) readingContext.getBean("recordingPacker");
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final CountDownLatch latch = new CountDownLatch(1);
        recordingSaver.setRecordingSaver(new RecordingSaver() {
            @Override
            public void save(byte[] buffer) {
                try {
                    bos.write(buffer);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                latch.countDown();
            }

            @Override
            public void close() throws IOException { }
        });

        HelloService helloService = readingContext.getBean(HelloService.class);

        logger.info(helloService.sayGoodbye("Billy"));
        logger.info("done in main");
        readingContext.close();
        latch.await();

        // Assert that a file was created
        assertEquals(1316, bos.size());

        // Part 2: Read the recorded call from part 1 and replay it in a new context.
        AbstractApplicationContext replayingContext = new ClassPathXmlApplicationContext("spring-replayer-xstream-test.xml");

        XStreamRecordingUnpacker unpacker = new XStreamRecordingUnpacker(new TestRecordingLoader("Testrecording", bos.toByteArray()));
        CallReplayer callReplayer = replayingContext.getBean(CallReplayer.class);
        for (Iterator<Recording> it = unpacker.recordingsIterator(); it.hasNext(); ) {
            callReplayer.replay(it.next());
        }

        replayingContext.close();
    }
}

class TestRecordingLoader implements RecordingLoader {

    private final String name;
    private final byte[] bytes;

    public TestRecordingLoader(String name, byte[] bytes) {
        this.name = name;
        this.bytes = bytes;
    }

    @Override
    public Iterator<Recording> getContentIterator() {
        return new Iterator<Recording>() {
            private boolean hasNext = true;

            @Override
            public boolean hasNext() {
                if (!hasNext) {
                    return false;
                }
                hasNext = false;
                return true;
            }

            @Override
            public Recording next() {
                return new Recording().setBuffer(bytes).setName(name);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
}

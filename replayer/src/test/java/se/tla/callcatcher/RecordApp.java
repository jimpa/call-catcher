/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Jim Svensson <jimpa@tla.se>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package se.tla.callcatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import se.tla.callcatcher.domain.HelloService;

public class RecordApp {
    private static final Logger logger = LoggerFactory.getLogger(RecordApp.class);

    public static void main(String[] args) {
        AbstractApplicationContext context =
                new ClassPathXmlApplicationContext("spring-pointcuts.xml", "recorder.xml", "xstream-packer.xml", "tarfile-saver.xml", "testconfiguration.xml");
        context.registerShutdownHook();

        HelloService helloService = context.getBean(HelloService.class);

        // Perform the call that will be recorded.
        logger.info(helloService.sayHello("Billy"));
        logger.info(helloService.sayGoodbye("Billy"));
        try {
            helloService.willThrowUp("Poor Billy");
        } catch (Exception e) {
            logger.info("Got expected exception: {}", e.getMessage());
        }
        logger.info("done in main");
        context.close();
    }
}
